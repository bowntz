/*

    Bowntz -- an audio-visual pseudo-physical simulation of colliding circles
    Copyright (C) 2010-2023 Claude-Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_opengl.h>

#include <sndfile.h>

#define pi 3.141592653589793

#define ITD_MAX 0.00066

#define WINDOW_WIDTH 512
#define WINDOW_HEIGHT 512

#define RECORD_WIDTH 1920
#define RECORD_HEIGHT 1080

typedef int ID;
typedef int COL;

#define BALL_COUNT 64

// maximum number of collisions per audio sample (for emergency stop)
#define COLLISION_LIMIT 64

typedef double R;

typedef double _Complex C;

static inline R dot(C u, C v)
{
  return creal(u) * creal(v) + cimag(u) * cimag(v);
}

const R
minSize = 0.02,
maxSize = 0.8,
minDensity = 40,
maxDensity = 60,
spawnThreshold = 0.1,
spawnEnergy = spawnThreshold / 8,
restartThreshold = spawnThreshold / 16,
elasticity = 0.99,
decay = 0.0001,
latency = 1 / 30;

struct Ball
{
  R mass;
  R radius;
  C position;
  C velocity;
  R ring;
  C freq;
  R iid;

  C lphase;
  C rphase;
  int rptr;
  R lbuf[128];
  R rbuf[128];
};

struct Collision
{
  R time;
  ID ball1;
  ID ball2;
};

int collision_compare(const void *a, const void *b)
{
  const struct Collision *x = a;
  const struct Collision *y = b;
  return (x->time > y->time) - (x->time < y->time);
}

static inline R kineticEnergy(const struct Ball *b)
{
  return 0.5 * b->mass * dot(b->velocity, b->velocity);
}

static inline bool involves(ID i, const struct Collision *c)
{
  return i == c->ball1 || i == c->ball2;
}

static inline void offset(R t, struct Collision *c)
{
  c->time += t;
}

void collideXside(R r, struct Ball *b1, struct Ball *b2)
{
  C n = (b1->position - b2->position) / r; // normal vector
  C t = I * n; // tangent vector
  C v1 = elasticity * dot(b1->velocity, t) * t +
    ( b1->mass * dot(b1->velocity, n)
    + b2->mass * dot(b2->velocity, n)
    + elasticity * b2->mass * dot(b2->velocity - b1->velocity, n)
    ) / (b1->mass + b2->mass) * n;
  C v2 = elasticity * dot(b2->velocity, t) * t +
    ( b2->mass * dot(b2->velocity, n)
    + b1->mass * dot(b1->velocity, n)
    + elasticity * b1->mass * dot(b1->velocity - b2->velocity, n)
    ) / (b1->mass + b2->mass) * n;
  R e1 = 0.5 * b1->mass * dot(v1, v1) - kineticEnergy(b1);
  R e2 = 0.5 * b2->mass * dot(v2, v2) - kineticEnergy(b2);
  b1->velocity = v1;
  b1->ring += fabs(e1);
  b2->velocity = v2;
  b2->ring += fabs(e2);
}

static inline void collideOutside(struct Ball *b1, struct Ball *b2)
{
  collideXside(b2->radius + b1->radius, b1, b2);
}

static inline void collideInside(struct Ball *b1, struct Ball * b2)
{
  collideXside(fabs(b2->radius - b1->radius), b1, b2);
}

static inline bool outside(const struct Ball *b1, const struct Ball *b2)
{
  C d = b2->position - b1->position;
  R r = b2->radius + b1->radius;
  return dot(d, d) > r * r;
}

static inline bool inside(const struct Ball *b1, const struct Ball *b2)
{
  C d = b2->position - b1->position;
  R r = b2->radius - b1->radius;
  return b1->radius < b2->radius && dot(d, d) < r * r;
}

static inline bool intersects(const struct Ball *b1, const struct Ball *b2)
{
  return ! (outside(b1, b2) || inside(b1, b2) || inside(b2, b1));
}

static inline bool approaching(const struct Ball *b1, const struct Ball *b2)
{
  return dot(b2->position - b1->position, b1->velocity - b2->velocity) > 0;
}

#define RINGBUFFER_SIZE 12

enum state_t { state_fill = 0, state_full };

struct World
{
  SDL_Window* window;
  SDL_GLContext context;
  bool interactive;
  bool fullscreen;
  bool record;
  int verbose;
  enum state_t state;
  R samplerate;
  int swapinterval;
  R framerate;
  R msperframe;
  R decay;
  R video;
  R spawn;
  R now;
  ID ball_count;
  COL collision_count;
  COL new_collision_count;
  int ringbuffer_read;
  int ringbuffer_write;
  struct Ball bounds;
  struct Ball balls[BALL_COUNT];
  struct Collision collision[BALL_COUNT * (BALL_COUNT - 1)];
  struct Collision new_collision[BALL_COUNT * 4];
  struct Ball ringbuffer[RINGBUFFER_SIZE][BALL_COUNT];
  ID ringbuffer_count[RINGBUFFER_SIZE];
};

R worldEnergy(const struct World *w)
{
  R sum = 0;
  for (ID b = 0; b < w->ball_count; ++b)
  {
    sum += kineticEnergy(&w->balls[b]);
  }
  return sum;
}

struct World world = {0};

void collides(struct World *w, ID i, const struct Ball *bi, ID j, const struct Ball *bj)
{
#define X(t) { if (w->new_collision_count < BALL_COUNT * 4 && 0 <= t) { struct Collision c = { t, i, j }; w->new_collision[w->new_collision_count++] = c; } }
  R ri = bi->radius;
  R rj = bj->radius;
  C qi = bi->position;
  C qj = bj->position;
  C vi = bi->velocity;
  C vj = bj->velocity;
  R a = dot(vi, vi) + dot(vj, vj) - 2 * dot(vi, vj);
  R b = dot(vi, qj) + dot(qi, vj) - dot(qi, vi) - dot(qj, vj);
  R c = a;
  if (outside(bi, bj))
  {
    R d1 = dot(qi, qi) + dot(qj, qj) - 2 * dot(qi, qj) - (ri + rj) * (ri + rj);
    R disc1 = (-2 * b) * (-2 * b) - 4 * c * d1;
    R t1 = 0.5 * (2 * b - sqrt(disc1)) / a;
    R t2 = 0.5 * (2 * b + sqrt(disc1)) / a;
    if (! approaching(bi, bj))
    {
    }
    else if (disc1 > 0)
    {
      X(t1)
      X(t2)
    }
    else if (disc1 == 0)
    {
      X(t1)
    }
    else
    {
    }
  }
  else if (inside(bi, bj) || inside(bj, bi) || approaching(bi, bj))
  {
    R d2 = dot(qi, qi) + dot(qj, qj) - 2 * dot(qi, qj) - (ri - rj) * (ri - rj);
    R disc2 = (-2 * b) * (-2 * b) - 4 * c * d2;
    R t3 = 0.5 * (2 * b - sqrt(disc2)) / a;
    R t4 = 0.5 * (2 * b + sqrt(disc2)) / a;
    if (disc2 > 0)
    {
      X(t3)
      X(t4)
    }
    else if (disc2 == 0)
    {
      X(t3)
    }
    else
    {
    }
  }
  else
  {
  }
#undef X
}

void insertBall(struct World *w, const struct Ball *b)
{
  if (w->ball_count < BALL_COUNT)
  {
    ID i = w->ball_count;
    w->balls[w->ball_count++] = *b;
    w->new_collision_count = 0;
    for (ID j = 0; j < i; ++j)
    {
      collides(w, i, b, j, &w->balls[j]);
    }
    for (COL k = 0; k < w->new_collision_count; ++k)
    {
      offset(w->now, &w->new_collision[k]);
    }
  }
}

static inline void advanceBalls(struct World *w, R dt)
{
  R r = pow(decay, dt);
  for (ID b = 0; b < w->ball_count; ++b)
  {
    w->balls[b].position += w->balls[b].velocity * dt;
    w->balls[b].ring *= r;
  }
}

static inline void triggerBall(struct Ball *b, R amp)
{
  R iid = 1.0/sqrt(1 + b->iid * fabs(creal(b->position)) / (cabs(b->position) + 1e-20));
  R itd = ITD_MAX * creal(b->position) / (cabs(b->position) + 1e-20);
  int samples = fmin(fmax(0.5 * world.samplerate * itd, -63), 63);
  b->lbuf[(b->rptr + 64 + samples) % 128] += itd > 0 ? amp * iid : amp / iid;
  b->rbuf[(b->rptr + 64 - samples) % 128] += itd > 0 ? amp / iid : amp * iid;
}

int restart_counter = 0;

void restart(struct World *w)
{
  w->state = state_fill;
  w->now = 0;
  w->ball_count = 1;
  w->collision_count = 0;
  w->new_collision_count = 0;
  w->balls[0].position = 0;
  w->balls[0].velocity = 0;
  if (w->verbose)
  {
    printf("restart %d\n", restart_counter++);
    fflush(stdout);
  }
}

void update(struct World *w, R target_time)
{
  int collision_counter = 0;
  while (w->now < target_time)
  {
    // precondition: collisions are sorted by time ascending, past is pruned, first collision is event to process
    if (w->state == state_fill && 0 < w->collision_count && w->collision[0].time <= target_time)
    {
      ID b1 = w->collision[0].ball1;
      ID b2 = w->collision[0].ball2;
      R dt = w->collision[0].time - w->now;
      if (++collision_counter == COLLISION_LIMIT)
      {
        // patch for 100% CPU freeze bug (too many collisions in too little time)
        if (w->verbose)
        {
          printf("%2d %2d %.18e\n", b1, b2, dt);
          printf("frozen\n");
          fflush(stdout);
        }
        w->state = state_full;
        continue;
      }
      struct Ball ball1 = w->balls[b1];
      struct Ball ball2 = w->balls[b2];
      advanceBalls(w, dt);
      w->now = w->collision[0].time;
      if (outside(&ball1, &ball2))
      {
        collideOutside(&ball1, &ball2);
      }
      else if (inside(&ball1, &ball2))
      {
        collideInside(&ball1, &ball2);
      }
      else if (inside(&ball2, &ball1))
      {
        collideInside(&ball2, &ball1);
      }
      else
      {
        // spurious collision?
      }
      triggerBall(&ball1, ball1.ring * ball1.mass);
      triggerBall(&ball2, ball2.ring * ball2.mass);
      w->balls[b1] = ball1;
      w->balls[b2] = ball2;
      // delete collisions that have been invalidated
      COL o = 0;
      for (COL k = 0; k < w->collision_count; ++k)
      {
        if (involves(b1, &w->collision[k]) || involves(b2, &w->collision[k]))
        {
        }
        else
        {
          w->collision[o] = w->collision[k];
          ++o;
        }
      }
      w->collision_count = o;
      // calculate new collisions
      w->new_collision_count = 0;
      for (ID i = 0; i < w->ball_count; ++i)
      {
        if (b1 == i || b2 == i)
        {
          continue;
        }
        collides(w, i, &w->balls[i], b1, &ball1);
        collides(w, i, &w->balls[i], b2, &ball2);
      }
      collides(w, b1, &ball1, b2, &ball2);
      for (COL k = 0; k < w->new_collision_count; ++k)
      {
        offset(w->now, &w->new_collision[k]);
      }
      // merge sorted lists
      qsort(&w->new_collision[0], w->new_collision_count, sizeof(w->new_collision[0]), collision_compare);
      COL total = w->collision_count + w->new_collision_count;
      COL i = w->collision_count;
      COL j = w->new_collision_count;
      for (COL k = total; k > 0; --k)
      {
        if (i > 0 && j > 0)
        {
          if (w->collision[i - 1].time > w->new_collision[j - 1].time)
          {
            w->collision[k - 1] = w->collision[i - 1];
            --i;
          }
          else
          {
            w->collision[k - 1] = w->new_collision[j - 1];
            --j;
          }
        }
        else if (i > 0)
        {
          w->collision[k - 1] = w->collision[i - 1];
          --i;
        }
        else if (j > 0)
        {
          w->collision[k - 1] = w->new_collision[j - 1];
          --j;
        }
      }
      w->collision_count = total;
    }
    else
    {
      advanceBalls(w, target_time - w->now);
      w->now = target_time;
    }
  }
}

void reshapecb(int width, int height)
{
  printf("%dx%d\n", width, height);
  fflush(stdout);
  glViewport(0, 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  R s = 1.125;
  if (width < height)
  {
    R v = s * height / width;
    glOrtho(-s, s, -v, v, -1, 1);
    world.bounds.radius = hypot(s, v);
  }
  else
  {
    R v = s * width / height;
    glOrtho(-v, v, -s, s, -1, 1);
    world.bounds.radius = hypot(s, v);
  }
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glLineWidth(3);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

float costable[144];

static inline float costab(int i)
{
  i %= 144;
  i += 144;
  i %= 144;
  return costable[i];
}

static inline float sintab(int i)
{
  return costab(i - 144 / 4);
}

float vertex[144 * 2 * 2];

void drawBall(const struct Ball *b)
{
  int n = 144;
  float r = b->radius;
  float _Complex p = b->position;
  float m = b->mass;
  float d = m / (r * r);
  float a = fminf((d - minDensity) / (maxDensity - minDensity), 1);
  if (d > maxDensity)
  {
    glColor3f(0, 0.7, 1);
  }
  else
  {
    float x = fmaxf(a, 0);
    float y = fmaxf(1 - a, 0);
    float z = fmaxf(x, y);
    glColor3f(x / z, y / z, z);
  }
  int v = 0;
  {
    float m1 = 1 + 5 * b->ring * costab(16 * 0) / r;
    float x = r * m1 * costab(0) + creal(p);
    float y = r * m1 * sintab(0) + cimag(p);
    vertex[v++] = x;
    vertex[v++] = y;
  }
  for (int i = 1; i < n; ++i)
  {
    float m = 1 + 5 * b->ring * costab(16 * i) / r;
    float x = r * m * costab(i) + creal(p);
    float y = r * m * sintab(i) + cimag(p);
    vertex[v++] = x;
    vertex[v++] = y;
    vertex[v++] = x;
    vertex[v++] = y;
  }
  {
    float m1 = 1 + 5 * b->ring * costab(16 * n) / r;
    float x = r * m1 * costab(n) + creal(p);
    float y = r * m1 * sintab(n) + cimag(p);
    vertex[v++] = x;
    vertex[v++] = y;
  }
  glDrawArrays(GL_LINES, 0, v / 2);
}

int underrun_counter = 0;
int overrun_counter = 0;

Uint64 then = 0;
double ms0 = 0, ms1 = 0, ms2 = 0;

void displaycb(void)
{
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  if (world.ringbuffer_read >= 0)
  {
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, vertex);
    for (ID b = 0; b < world.ringbuffer_count[world.ringbuffer_read % RINGBUFFER_SIZE]; ++b)
    {
      drawBall(&world.ringbuffer[world.ringbuffer_read % RINGBUFFER_SIZE][b]);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
  }
  if (world.ringbuffer_write - world.ringbuffer_read > 1)
  {
    ++world.ringbuffer_read;
  }
  else
  {
    // buffer empty, what do
    if (world.verbose)
    {
      printf("underrun %d\n", underrun_counter++);
      fflush(stdout);
    }
    world.framerate /= 0.999;
  }
  if (! world.record)
  {
    glFinish();
    SDL_GL_SwapWindow(world.window);
    Uint64 now = SDL_GetTicks/*64*/();
    Uint64 ms = now - then;
    if (then > 0)
    {
      ms0 += 1;
      ms1 += ms;
      ms2 += ms * ms;
      double mean = ms1 / ms0;
      double stddev = sqrt((ms0 * ms2 - ms1 * ms1) / (ms0 * (ms0 - 1)));
      double error = stddev / sqrt(ms0);
      if (ms1 > 1000)
      {
        world.msperframe *= 0.99;
        world.msperframe += 0.01 * mean;
        world.framerate = 1000 / world.msperframe;
      }
      if (world.verbose >= 2)
      {
        fprintf(stderr, "\r%d %.4f %.3f [ %.3f , %.3f ] ", (int) ms, world.msperframe, world.framerate, 1000.0 / (mean + 1.96 * error), 1000.0 / (mean - 1.96 * error));
      }
    }
    then = now;
  }
}

static inline R randomRIO(R mi, R ma)
{
  R u = rand() / (R) RAND_MAX;
  return mi + u * (ma - mi);
}

void spawn(struct World *w)
{
  if (w->state == state_full)
  {
    bool far = true;
    for (ID b = 1; b < w->ball_count; ++b)
    {
      far &= outside(&w->balls[b], &w->bounds);
      w->balls[b].velocity *= 1.1;
    }
    if (far)
    {
      restart(w);
    }
  }
  else if (w->ball_count < BALL_COUNT)
  {
    if (worldEnergy(w) < spawnThreshold)
    {
      R d = randomRIO(minDensity, maxDensity);
      R r = randomRIO(minSize, maxSize);
      R pr = randomRIO(0, 1);
      R pa = randomRIO(-pi, pi);
      R va = randomRIO(-pi, pi);
      R m = d * r * r;
      R px = pr * cos(pa);
      R py = pr * sin(pa);
      R vr = sqrt(2 * spawnEnergy / m);
      R vx = vr * cos(va);
      R vy = vr * sin(va);
      R hz = 50 / (r * r);
      C freq = cexp(2 * pi * I * hz / w->samplerate) * w->decay;
      R iid = pow(hz / 1000, 0.8);
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wmissing-field-initializers"
      struct Ball s = { m, r, px + I * py, vx + I * vy, 0, freq, iid /*, ... */ };
  #pragma GCC diagnostic pop
      bool ok = true;
      for (ID b = 0; b < w->ball_count; ++b)
      {
        if (intersects(&s, &w->balls[b]))
        {
          ok = false;
          break;
        }
      }
      if (ok)
      {
        insertBall(w, &s);
        // FIXME merge sorted lists dropping first collision
        // qsort(&w->new_collision[0], w->new_collision_count, sizeof(w->new_collision[0]), collision_compare);
        memcpy(&w->collision[w->collision_count], &w->new_collision[0], sizeof(w->collision[0]) * w->new_collision_count);
        w->collision_count += w->new_collision_count;
        qsort(&w->collision[0], w->collision_count, sizeof(w->collision[0]), collision_compare);
        if (w->ball_count == BALL_COUNT)
        {
          if (w->verbose)
          {
            printf("full\n");
            fflush(stdout);
          }
        }
      }
    }
  }
  else
  {
    w->state = state_full;
  }
}

void audio1(struct World *w, float out[2])
{
  w->video += 1;
  if (w->video >= w->samplerate / w->framerate)
  {
    w->video = 0;
    if (w->ringbuffer_write - w->ringbuffer_read < RINGBUFFER_SIZE)
    {
      memcpy(w->ringbuffer[w->ringbuffer_write % RINGBUFFER_SIZE], &w->balls[0], sizeof(w->balls[0]) * w->ball_count);
      w->ringbuffer_count[w->ringbuffer_write % RINGBUFFER_SIZE] = w->ball_count;
      ++w->ringbuffer_write;
    }
    else
    {
      // buffer full
      if (w->verbose)
      {
        printf("overrun %d\n", overrun_counter++);
        fflush(stdout);
      }
      w->framerate *= 0.999;
    }
  }
  w->spawn += 1;
  if (w->spawn >= w->samplerate / 8)
  {
    w->spawn = 0;
    spawn(w);
  }
  update(w, w->now + 1.0 / w->samplerate);
  out[0] = 0;
  out[1] = 0;
  for (ID b = 1; b < w->ball_count; ++b)
  {
    w->balls[b].lphase += w->balls[b].lbuf[w->balls[b].rptr];
    w->balls[b].rphase += w->balls[b].rbuf[w->balls[b].rptr];
    w->balls[b].lbuf[w->balls[b].rptr] = 0;
    w->balls[b].rbuf[w->balls[b].rptr] = 0;
    ++w->balls[b].rptr;
    w->balls[b].rptr %= 128;
    w->balls[b].lphase *= w->balls[b].freq;
    w->balls[b].rphase *= w->balls[b].freq;
    out[0] += cimag(w->balls[b].lphase);
    out[1] += cimag(w->balls[b].rphase);
  }
  out[0] = tanhf(out[0]);
  out[1] = tanhf(out[1]);
}

void audio(void *userdata, Uint8 *stream, int len)
{
  (void) userdata;
  float *b = (float *) stream;
  int m = len / sizeof(float) / 2;
  int k = 0;
  for (int i = 0; i < m; ++i)
  {
    float out[2];
    audio1(&world, out);
    b[k++] = out[0];
    b[k++] = out[1];
  }
}

void toggleFullscreen(void)
{
  world.fullscreen = ! world.fullscreen;
  SDL_SetWindowFullscreen(world.window, world.fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
  SDL_ShowCursor(world.fullscreen ? SDL_DISABLE : SDL_ENABLE);
  int w, h;
  SDL_GetWindowSize/*InPixels*/(world.window, &w, &h);
  // SDL bug? returns full screen size when ending fullscreen
  if (world.fullscreen)
  {
    reshapecb(w, h);
  }
  else
  {
    reshapecb(WINDOW_WIDTH, WINDOW_HEIGHT);
  }
}

bool interact(void)
{
  bool running = true;
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0)
  {
    if (! world.interactive)
    {
      continue;
    }
    switch (event.type)
    {
      case SDL_QUIT:
      {
        running = false;
        break;
      }
      case SDL_KEYDOWN:
      {
        switch (event.key.keysym.sym)
        {
          case SDLK_q:
          case SDLK_ESCAPE:
          {
            running = false;
            break;
          }
          case SDLK_f:
          case SDLK_F11:
          {
            toggleFullscreen();
            break;
          }
        }
        break;
      }
    }
  }
  return running;
}

void video(void)
{
  while (interact())
  {
    displaycb();
  }
}

void main1(void)
{
  if (interact())
  {
    displaycb();
  }
}

unsigned char rgb[RECORD_HEIGHT][RECORD_WIDTH][3];

int main(int argc, char **argv)
{
  int seed = time(0);
  world.interactive = 1;
  for (int i = 1; i < argc; ++i)
  {
    if (strncmp("--seed=", argv[i], 7) == 0)
    {
      seed = atoi(argv[i] + 7);
    }
    else if (strncmp("--fs=", argv[i], 5) == 0)
    {
      world.fullscreen = atoi(argv[i] + 5);
    }
    else if (strncmp("--fps=", argv[i], 6) == 0)
    {
      world.framerate = atof(argv[i] + 6);
    }
    else if (strncmp("--swap=", argv[i], 7) == 0)
    {
      world.swapinterval = atoi(argv[i] + 7);
    }
    else if (strncmp("--record=", argv[i], 9) == 0)
    {
      world.record = atoi(argv[i] + 9);
    }
    else if (strncmp("--verbose=", argv[i], 10) == 0)
    {
      world.verbose = atoi(argv[i] + 10);
    }
    else if (strncmp("--interactive=", argv[i], 14) == 0)
    {
      world.interactive = atoi(argv[i] + 14);
    }
  }
  srand(seed);
  printf("Bowntz (AGPL3) 2010-2023 Claude Heiland-Allen\n");
  printf("PRNG seed %d\n", seed);
  fflush(stdout);
  for (int i = 0; i < 144; ++i)
  {
    costable[i] = cos(2 * pi * i / 144.);
  }
  SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO);
  SDL_AudioSpec want, have;
  want.freq = 44100;
  want.format = AUDIO_F32;
  want.channels = 2;
  want.samples = 4096;
  want.callback = audio;
  SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if (have.freq > 192000 || have.format != AUDIO_F32 || have.channels != 2)
  {
    printf("want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
    printf("have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);
    printf("error: bad audio parameters\n");
    return 1;
  }
  world.samplerate = have.freq;
  if (world.swapinterval <= 0)
  {
    world.swapinterval = 1;
  }
  if (world.framerate == 0)
  {
    SDL_DisplayMode mode = {0};
    if (0 == SDL_GetDesktopDisplayMode(0, &mode))
    {
      printf("desktop 0 %dx%dp%d\n", mode.w, mode.h, mode.refresh_rate);
      world.framerate = mode.refresh_rate;
    }
  }
  if (world.framerate == 0)
  {
    world.framerate = 60; // FIXME
  }
  world.framerate /= world.swapinterval;
  world.msperframe = 1000 / world.framerate;
  world.ringbuffer_read = world.record ? -1 : -RINGBUFFER_SIZE / 2;
  world.ringbuffer_write = 0;
  world.decay = pow(1e-3, 1.0 / (0.7 * world.samplerate));
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
  struct Ball b = { 1e6, 1, 0, 0, 0, cexp(2 * pi * I * 50 / world.samplerate) * world.decay, pow(50. / 1000, 0.8) /*, ... */ };
#pragma GCC diagnostic pop
  insertBall(&world, &b);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  world.window = SDL_CreateWindow("Bowntz", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, world.record ? RECORD_WIDTH : WINDOW_WIDTH, world.record ? RECORD_HEIGHT : WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
  if (! world.window)
  {
    printf("error: couldn't create SDL2 window\n");
    return 1;
  }
  world.context = SDL_GL_CreateContext(world.window);
  if (! world.context)
  {
    printf("error: couldn't create OpenGL context\n");
    return 1;
  }
  if (world.record)
  {
    reshapecb(RECORD_WIDTH, RECORD_HEIGHT);
    char cmd[1000];
    snprintf(cmd, sizeof(cmd), "ffmpeg -framerate 60 -r 60 -i - -vf vflip -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 -y bowntz-%d.mkv", seed);
    FILE *ppm = popen(cmd, "w");
    if (! ppm)
    {
      return 1;
    }
    SF_INFO info = { 0, 44100, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
    snprintf(cmd, sizeof(cmd), "bowntz-%d.wav", seed);
    SNDFILE *wav = sf_open(cmd, SFM_WRITE, &info);
    if (! wav)
    {
      return 1;
    }
    glReadBuffer(GL_BACK);
    for (int frame = 0; restart_counter == 0; ++frame)
    {
      main1();
      glReadPixels(0, 0, RECORD_WIDTH, RECORD_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, rgb);
      fprintf(ppm, "P6\n%d %d\n255\n", RECORD_WIDTH, RECORD_HEIGHT);
      fflush(ppm);
      fwrite(rgb, sizeof(rgb), 1, ppm);
      fflush(ppm);
      float buffer[44100 / 60 * 2];
      audio(0, (void *) buffer, sizeof(buffer));
      sf_writef_float(wav, buffer, 44100 / 60);
    }
    sf_close(wav);
    pclose(ppm);
    snprintf(cmd, sizeof(cmd), "fdkaac -b 384k bowntz-%d.wav", seed);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "ffmpeg -i bowntz-%d.mkv -i bowntz-%d.m4a -codec:v copy -codec:a copy -movflags +faststart bowntz-%d.mp4", seed, seed, seed);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "rm bowntz-%d.wav bowntz-%d.m4a bowntz-%d.mkv", seed, seed, seed);
    system(cmd);
    return 0;
  }
  SDL_GL_SetSwapInterval(world.swapinterval);
  world.fullscreen = ! world.fullscreen;
  toggleFullscreen();
  // start audio processing
  if (world.verbose)
  {
    printf("restart %d\n", restart_counter++);
    fflush(stdout);
  }
  SDL_PauseAudioDevice(dev, 0);
#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop(main1, 0, 1);
#else
  video();
#endif
  SDL_DestroyWindow(world.window);
  world.window = 0;
  SDL_Quit();
  return 0;
}
