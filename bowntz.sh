#!/bin/bash
#set -x
cd "$(dirname "$(readlink -f "${0}")")"
while true
do
# detect screens
SCREENS="$(xrandr | grep ' connected ' | sed 's/ .*$//g')"
INTERNAL="$(echo "${SCREENS}" | tr ' ' '\n' | grep    '^LVDS' | head -n 1 | tr -d '[:space:]' )"
EXTERNAL="$(echo "${SCREENS}" | tr ' ' '\n' | grep -v '^LVDS' | head -n 1 | tr -d '[:space:]' )"
SCREEN_OFF=""
if [ "x${EXTERNAL}" = "x" ]
then
  if [ "x${INTERNAL}" = "x" ]
  then
    echo "bowntz: no screens recognized"
    exit 1
  else
    echo "bowntz: using internal screen"
    SCREEN="${INTERNAL}"
  fi
else
  SCREEN="${EXTERNAL}"
  if [ "x${INTERNAL}" = "x" ]
  then
    echo "bowntz: using external screen"
  else
    echo "bowntz: using external screen (internal detected too)"
    SCREEN_OFF="${INTERNAL}"
  fi
fi
# set up screens
if [ "x${SCREEN_OFF}" = 'x' ]
then
  xrandr --output "${SCREEN}" --auto
else
  xrandr --output "${SCREEN}" --auto --output "${SCREEN_OFF}" --off
fi
killall light-locker
xset -dpms s off
xsetroot -solid black -cursor_name none
# get screen resolution
RESOLUTION="$(xrandr | grep "^${SCREEN} connected " | sed "s/primary //g" | sed "s/^${SCREEN} connected \([^ ]*\) .*$/\1/")"
if [ "x${RESOLUTION}" = "x" ]
then
  echo "bowntz: failed to detect screen resolution"
  exit 1
fi
echo "${RESOLUTION}" | tr 'x+' '  ' | (
  read SCREENW SCREENH SCREENX SCREENY
  echo "resolution '$SCREENW' '$SCREENH' '$SCREENX' '$SCREENY'"
  # set framerate as high as possible
  xrandr --output "${SCREEN}" --mode "${SCREENW}x${SCREENH}" --rate 1000
  # move mouse cursor to top right hand corner, and hide it
  ( while true ; do xsetroot -cursor blank.xbm blank.xbm ; ./warp 10000 -10000 ; sleep 10 ; done ) &
  # geometry of window centered on screen will be WxH+X+Y
  ./bowntz --fs=1 --interactive=0 
)
echo "bowntz: done"
sleep 5
# restart from scratch if it crashes, to ensure clean setup
killall --user bowntz
done
